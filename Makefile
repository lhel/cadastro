build_app:
	docker build --no-cache -t cadastro .

wait_for_postgres_up:
	until docker-compose run --rm check_postgres_up > /dev/null; do \
	    echo "Waiting for db to come up..."; \
	done

run_dev_backend:
	docker-compose up -d db

run_dev_server:
	$(MAKE) run_dev_backend
	$(MAKE) wait_for_postgres_up
	docker-compose up cadastro

stop_server:
	docker-compose stop	-t 0 cadastro db
	docker-compose rm -f cadastro db

run_test_backend:
	docker-compose up -d db_test

wait_for_postgres_test:
	until docker-compose run --rm check_postgres_test_up > /dev/null; do \
	    echo "Waiting for db to come up..."; \
	done

run_tests:
	$(MAKE) run_test_backend
	$(MAKE) wait_for_postgres_test
	docker-compose up cadastro_test

stop_postgres_test:
	docker-compose stop	-t 0 db_test

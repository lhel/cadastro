# Cadastro

API to register and create users.

## Default Setup

Clone this repository:
    
    git clone https://gitlab.com/lhel/cadastro.git

Create a virtual environment:

    python3 -m venv cadastro_env
    source cadastro_env/bin/activate

Install dependencies:
    
    pip install -r requirements.txt

Set the environment variables:

    export FLASK_APP=cadastro.py

### Running development server
    
Run the app:
    
    flask run

Go to http://localhost:5000/apidocs for see the endpoints.

### Running tests

From terminal, run:

    pytest

### API Documentation

Go to http://localhost:5000/apidocs.

## Setup with Docker and Postgresql

### Installation

Install [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).

### First Build

Clone this repository:
    
    git clone https://gitlab.com/lhel/cadastro.git

From a terminal, in the root project:
    
    make build_app

### Running Dev Server

    make run_dev_server

Go to http://localhost:8000/apidocs for see the endpoints.

### Stopping Dev Server

    make stop_dev_server

### Running Tests

    make run_tests

# Setting Authorization in HTTP Request Header

    Authorization: Bearer your-token

Example:

    Authorization: Bearer gcxKSb3ZQGWiTI9juh/upM08wkrXxr9L

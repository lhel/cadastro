from app.auth import bp as app
from app.errors import error_response


@app.errorhandler(404)
def handle_404(error):
    return error_response(404, 'User not found')

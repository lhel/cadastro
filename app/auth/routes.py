from flask import request, jsonify

from app import db
from app.auth import bp as app
from app.errors import bad_request
from app.models import User


@app.route('/login', methods=["POST"])
def login():
    """Endpoint responsável pelo login no sistema.
    ---
    parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            properties:
                username:
                    type: string
                password:
                    type: string
    responses:
        200:
            description: Retorna um token para acesso as views que requerem login.
        400:
            description: Username ou password inválido
    """

    data = request.get_json() or {}
    if 'username' not in data and 'password' not in data:
        return bad_request('username or password required')
    user = User.query.filter_by(username=data['username']).first_or_404()
    if not user.check_password(data['password']):
        return bad_request('Invalid username or password')
    token = user.get_token()
    db.session.commit()
    return jsonify({'token': token})

from flask import Blueprint

bp = Blueprint('auth', __name__)

from app.auth import routes, auth_token, error_handlers

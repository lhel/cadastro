from sqlalchemy.exc import IntegrityError

from app.api import bp as app
from app.errors import error_response


@app.errorhandler(IntegrityError)
def handle_user_already_exists(error):
    msg = "User already exists. Please use a different username and/or email address."
    return error_response(409, msg)


@app.errorhandler(404)
def handle_404(error):
    return error_response(404, 'User not found')

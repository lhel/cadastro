from flask import request, jsonify, url_for

from app import db
from app.api import bp as app
from app.models import User
from app.errors import bad_request
from app.auth.auth_token import token_auth


@app.route('/users/<username>', methods=['GET'])
@token_auth.login_required
def get_user(username):
    """Retorna informações de um usuário.
    ---
    parameters:
        - in: path
          name: username
          type: string
          required: true
        - in: header
          name: Authorization
          schema:
              type: string
          required: true
    responses:
        200:
            description: Retorna informações de um usuário pelo id.
        404:
            description: Usuário não encontrado.
    """
    user = User.query.filter_by(username=username).first_or_404()
    return jsonify({'username': user.username, 'email': user.email})


@app.route('/users', methods=['POST'])
def create_user():
    """Cadastra um novo usuário.
    ---
    parameters:
        - in: body
          name: body
          required: true
          schema:
            type: object
            properties:
                username:
                    type: string
                email:
                    type: string
                password:
                    type: string
    responses:
        201:
            description: Retorna o usuário inserido no database.
            examples:
                {"username": "john", "email": "john@gmail.com"}
        400:
            description: Deve incluir username, email e password.
    """
    data = request.get_json() or {}
    if 'username' not in data:
        return bad_request('must include username')
    if 'email' not in data:
        return bad_request('must include email')
    if 'password' not in data:
        return bad_request('must include password')
    user = User(username=data['username'], email=data['email'])
    user.set_password(data['password'])
    db.session.add(user)
    db.session.commit()
    response = jsonify({'username': user.username, 'email': user.email})
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_user', username=user.username)
    return response


@app.route('/users', methods=['GET'])
@token_auth.login_required
def get_users():
    """Retorna informações de todos os usuários cadastrados.
    ---
    parameters:
        - in: query
          name: page
          schema:
              type: integer
        - in: query
          name: per_page
          schema:
              type: integer
          description: A quantidade de items por página.
        - in: header
          name: Authorization
          schema:
              type: string
          required: true
    responses:
        200:
            description: Retorna informações de todos os usuários.
    """
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(page, per_page, 'api.get_users')
    return jsonify(data)

import pytest

from flask import url_for, json
from app.models import User


@pytest.mark.parametrize('user', [
    User(username='john', email="john@example.com"),
    User(username='susan', email="susan@example.com"),
    User(username='morris', email="morris@example.com"),
])
def test_get_user(client, db, user_bob, user):
    db.session.add(user)
    db.session.commit()
    resp = client.get(url_for('api.get_user', username=user.username),
                      auth_token=user_bob.token)
    data = json.loads(resp.data)
    assert data['username'] == user.username
    assert data['email'] == user.email


def test_get_user_not_found(client, db, user_bob):
    u = User(username='john', email="john@example.com")
    db.session.add(u)
    db.session.commit()
    resp = client.get(url_for('api.get_user', username='john doe'), auth_token=user_bob.token)
    assert resp.status_code == 404


def test_create_user(client, db):
    user = {'username': 'john', 'email': 'john@example.com',
            'password': 'pass'}
    resp = client.post(url_for('api.create_user'), data=json.dumps(user),
                       content_type='application/json')
    data = json.loads(resp.data)
    assert resp.status_code == 201
    assert data['username'] == user['username']
    assert data['email'] == user['email']


@pytest.mark.parametrize('user, expected_msg', [
    ({'email': 'john@example.com', 'password': 'pass'}, 'username'),
    ({'username': 'john', 'password': 'pass'}, 'email'),
    ({'email': 'john@example.com', 'username': 'john'}, 'password'),
])
def test_create_user_bad_request(client, db, user, expected_msg):
    resp = client.post(url_for('api.create_user'), data=json.dumps(user),
                       content_type='application/json')
    data = json.loads(resp.data)
    assert resp.status_code == 400
    assert data['message'] == 'must include {}'.format(expected_msg)


def test_login(client, db, user_bob):
    payload = {'username': user_bob.username, 'password': 'pass'}

    resp = client.post(url_for('auth.login'),
                       data=json.dumps(payload),
                       content_type='application/json')

    assert resp.status_code == 200


def test_login_user_not_found(client, db):
    payload = {'username': 'john', 'password': 'pass'}
    resp = client.post(url_for('auth.login'), data=json.dumps(payload),
                       content_type='application/json')

    assert resp.status_code == 404


def test_login_invalid_password(client, db, user_bob):
    payload = {'username': user_bob.username,
               'password': 'other'}

    resp = client.post(url_for('auth.login'),
                       data=json.dumps(payload),
                       content_type='application/json')
    data = json.loads(resp.data)

    assert resp.status_code == 400
    assert data['error'] == 'Bad Request'
    assert data['message'] == 'Invalid username or password'


def test_get_users(client, db, user_bob):
    users = [
        User(username='john', email="john@example.com"),
        User(username='susan', email="susan@example.com"),
        User(username='morris', email="morris@example.com"),
    ]
    db.session.add_all(users)
    db.session.commit()
    resp = client.get(url_for('api.get_users'), auth_token=user_bob.token)
    data = json.loads(resp.data)

    assert resp.status_code == 200
    assert data.get('users')

    for user in users:
        assert {'username': user.username, 'email': user.email} in data['users']


def test_get_users_pagination(client, db, user_bob):
    users = []
    for i in range(100):
        users.append(User(username='john{}'.format(i),
                          email='john{}example.com'.format(i)))
    db.session.add_all(users)
    db.session.commit()
    resp = client.get(url_for('api.get_users', page=4, per_page=14),
                      auth_token=user_bob.token)
    data = json.loads(resp.data)
    assert resp.status_code == 200
    assert data.get('users')
    assert data.get('_links')
    assert data.get('_meta')
    assert data['_meta']['page'] == 4
    assert data['_meta']['per_page'] == 14
    assert len(data['users']) == 14
    assert data['_meta']['total_users'] == 101

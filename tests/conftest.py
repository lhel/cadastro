import os
import tempfile

import pytest

from flask.testing import FlaskClient

from config import TestingConfig
from app import create_app, db as _db
from app.models import User


class AuthClient(FlaskClient):
    """A FlaskClient which can handle authentication."""

    def open(self, *args, **kwargs):
        auth_token = kwargs.pop('auth_token', None)
        if auth_token:
            if 'headers' not in kwargs:
                kwargs['headers'] = {}
            kwargs['headers']['Authorization'] = "Bearer {}".format(auth_token)

        return FlaskClient.open(self, *args, **kwargs)


@pytest.fixture
def app():
    _app = create_app(TestingConfig)

    with _app.app_context():
        yield _app


@pytest.fixture
def client(app):
    app.test_client_class = AuthClient
    with app.test_request_context():
        yield app.test_client()


@pytest.fixture
def db(app):
    db_fd, temp_file = tempfile.mkstemp()
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + temp_file
    _db.create_all()

    yield _db

    _db.session.remove()
    _db.drop_all()

    os.close(db_fd)


@pytest.fixture
def user_bob(db):
    bob = User(username='bob', email="bob@example.com")
    bob.set_password('pass')
    db.session.add(bob)
    db.session.commit()

    bob.get_token()
    db.session.commit()

    return bob

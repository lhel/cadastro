from app.models import User


def test_user_password(db):
    u = User(username='john', email='john@example.com')
    u.set_password('pass')
    assert u.check_password('pass')
    assert not u.check_password('anotherpass')

#!/bin/sh
while true; do
    flask db upgrade
    if [[ "$?" == "0" ]]; then
        break
    fi
    echo Upgrade command failed, retrying in 5 secs...
    sleep 5
done

if [ "$1" == "test" ]; then 
   pytest
else
    exec gunicorn -b :5000 --access-logfile - --error-logfile - cadastro:app
fi

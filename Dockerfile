FROM python:3.5-alpine

RUN adduser -D cadastro

WORKDIR /home/cadastro

RUN apk update && apk add git
RUN \
    apk add --no-cache postgresql-libs && \
    apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev
RUN git clone https://gitlab.com/lhel/cadastro .
RUN pip install --upgrade pip psycopg2 gunicorn
RUN pip install -r requirements.txt
RUN apk --purge del .build-deps

RUN chmod +x boot.sh

ENV FLASK_APP cadastro.py

RUN chown -R cadastro:cadastro ./
USER cadastro

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
